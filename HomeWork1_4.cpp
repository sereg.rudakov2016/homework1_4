﻿
#include <iostream>
#include <cmath>

class Field
{
public:

    Field& operator=(Field& other)
    {
        std::cout << "operator = ";    

        Value = other.Value;
        CoordRow = other.CoordRow;
        CoordCol = other.CoordCol;

        arr = new int* [other.row];
        for (int i = 0; i < row; ++i)
        {
            arr[i] = new int[col];
        }

        FieldPrint();

        return (*this);
    }


    Field(int Inrow, int Incol, char InValue, int InCoordRow, int InCoordCol) : row(Inrow), col(Incol)
    {
        arr = new int*[row];

        Value = InValue;
        CoordRow = InCoordRow;
        CoordCol = InCoordCol;

        for (int i = 0; i < row; ++i)
        {
            arr[i] = new int[col];            
        }

        FieldPrint();
    }

    void FieldPrint()
    {
        for (int i = 0; i < row; ++i)
        {
            std::cout << "\n";

            for (int j = 0; j < col; ++j)
            {
                std::cout << " ";

                arr[i][j] = 0;

                if (i == CoordRow && j == CoordCol)
                {
                    std::cout << Value;
                }
                else
                {
                    std::cout << arr[i][j];
                }
            }
        }
    }

    ~Field()
    {
        if (arr)
        {
            delete arr;
        }
    }

private:
    int** arr;
    int row;
    int col;
    char Value;
    int CoordRow;
    int CoordCol;
};



int main()
{
    std::cout << "f1  \n";
    Field f1(5, 5, 'p', 0, 0);
    std::cout << "\n";
    std::cout << "f2 \n";
    Field f2(5, 5, 'n', 4, 4);
    std::cout << "\n";
    f2 = f1;




}
